Para executar esse ambiente você deve ter instalado:
 
 a) A Virtualbox (https://www.virtualbox.org/wiki/Downloads)
 
 b) O Vagrant (https://www.vagrantup.com/downloads.html)
 
 c) O Ansible (https://www.ansible.com/resources/get-started)

Com  isso instalado apenas clone esse repositório: git clone https://jorge_rabello@bitbucket.org/jorge_rabello/ambiente_mysql_ansible.git

E após clonar exeute com vagrant up em uma linha de comando mais próxima (rsrsrsrs)

OBS: Altere o arquivo hosts apontando para sua home

[mysql]

172.17.177.10 ansible_user=vagrant ansible_ssh_private_key_file="/home/SUA_HOME/PASTA_ONDE_VC_CLONOU/.vagrant/machines/mysql/virtualbox/private_key"

Exemplo:

[mysql]

172.17.177.10 ansible_user=vagrant ansible_ssh_private_key_file="/home/jorge/Documents/infra/mysql/.vagrant/machines/mysql/virtualbox/private_key"

Para fazer o provisionamento com as configurações desses scripts utilizando o ansible execute

	 ansible-playbook provisioning.yml -i hosts 

-- Se por uma acaso ocorrer o famigerado erro unreachable (algo mais ou menos assim):

172.17.177.40 | UNREACHABLE! => {
    "changed": false,
    "msg": "Failed to connect to the host via ssh: vagrant@172.17.177.40: Permission denied (publickey,password).\r\n",
    "unreachable": true
}

Não entre em pânico, faça o seguinte:

1) Crie uma pasta chamada ssh-keys e acesse essa pasta

	mkdir ssh-keys
	
	cd ssh-keys

2) Gere as chaves ssh

	ssh-keygen -t rsa

3) Quando pedir a senha digite e confirme a senha vagrant

4) Garanta que a box está ligada não estiver de um vagrant up

5) Copie a chave ssh para a box 

	ssh-copy-id -i vagrant_id_rsa.pub vagrant@172.17.177.40

6) Tente executar novamente o comando

	 ansible-playbook provisioning.yml -i hosts 

OBS: Talvez esteja necessário remover o arquivo ~/.ssh/known_hosts

OBS II: Isso tudo funciona no Linux não testei no MS Windows nem em Macs


